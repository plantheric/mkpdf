# mkdpf

mkpdf is a tool to convert a simple markdown document to PDF.

mkpdf creates a PDF directly from the markdown without the intermediate step of converting to HTML. This results in a faster conversion and better quality output than many other solutions but means that inline HTML is not supported.

mkpdf runs as a command line utility on OS X and Linux, and also includes an OS X system service.

mkpdf is currently under development.

### Compatibility

mkpdf renders all the text components of the [original markdown syntax](http://daringfireball.net/projects/markdown/syntax). Support for images is currently limited.

There is currently no mechanism to change the layout and font choices other than changing the code. Loading these settings from a configuration file will be added later.
