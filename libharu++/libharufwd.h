//
//  libharufwd.h
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef mkpdf_libharufwd_h
#define mkpdf_libharufwd_h

typedef struct _HPDF_Doc_Rec  *HPDF_Doc;
typedef struct _HPDF_Dict_Rec  *HPDF_Dict;
typedef HPDF_Dict HPDF_Page;
typedef HPDF_Dict HPDF_Font;
typedef HPDF_Dict HPDF_Image;
typedef struct _HPDF_Rect HPDF_Rect;

#endif
