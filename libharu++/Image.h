//
//  Image.h
//  mkpdf
//
//  Created by Mark Bartlett on 14/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__libharu_Image__
#define __mkpdf__libharu_Image__

#include <vector>
#include <string>

#include "libharufwd.h"

namespace libharu {
	
class Document;
class Page;
	
class Image {
public:
	Image (HPDF_Image& image);

	void Draw(Page& page);
	void Position(float x, float y);
	
	float Width() const;
	float Height() const;
	
private:
	HPDF_Image mImage;
	float mX;
	float mY;
};

}

#endif /* defined(__mkpdf__libharu_Image__) */
