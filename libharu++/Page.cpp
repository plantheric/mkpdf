//
//  Page.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//
#include <iostream>

#include "Page.h"
#include "Text.h"
#include "Link.h"
#include "Rule.h"
#include "libharu++/Image.h"

#include "hpdf.h"

namespace libharu {
	
Page::Page(const HPDF_Page& pdfPage) {

	mPage = pdfPage;
	HPDF_Page_SetSize (mPage, HPDF_PAGE_SIZE_A4, HPDF_PAGE_PORTRAIT);
}

Page::Page() {

	mPage = NULL;
}


Page::~Page() {
	
}

Text& Page::GetText() {

	if (!mText) {
		mText = std::make_shared<Text>(Text(*this));
	}

	return *mText;
}

float Page::Width() {
	
	return HPDF_Page_GetWidth(mPage);
}

float Page::Height() {

	return HPDF_Page_GetHeight(mPage);
}

Page::operator HPDF_Page() {

	return mPage;
}

void Page::Close() {

	mText.reset();
	DrawLinks();
	DrawRules();
	DrawImages();
}

void Page::AddLink(const Link& link) {

	mLinks.push_back(std::make_shared<Link>(link));
}

void Page::DrawLinks() {

	for (auto link : mLinks) {
		
		HPDF_Page_CreateURILinkAnnot (mPage, link->Rect(), link->url.c_str());

		HPDF_Page_SetLineWidth (mPage, 1.0);
		const HPDF_UINT16 DASH_MODE[] = {2};
		HPDF_Page_SetDash (mPage, DASH_MODE, 1, 1);
		HPDF_Page_MoveTo (mPage, link->left, link->bottom);
		HPDF_Page_LineTo (mPage, link->right, link->bottom);
		HPDF_Page_Stroke (mPage);
	}
}

void Page::AddRule(const Rule& rule) {
	
	mRules.push_back(std::make_shared<Rule>(rule));
}

void Page::DrawRules() {
	
	for (auto rule : mRules) {
		
		HPDF_Page_SetLineWidth (mPage, 0.5);
		HPDF_Page_SetDash (mPage, NULL, 0, 0);
		HPDF_Page_MoveTo (mPage, rule->margin, rule->y);
		HPDF_Page_LineTo (mPage, Width() - rule->margin, rule->y);
		HPDF_Page_Stroke (mPage);
	}
	
}

void Page::AddImage(const Image& image) {
	
	mImages.push_back(std::make_shared<Image>(image));
}

void Page::DrawImages() {
	
	for (auto image : mImages) {
		
		image->Draw(*this);
	}
}
	
}
