//
//  Page.h
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Page__
#define __mkpdf__Page__

#include "libharufwd.h"

#include <list>
#include <memory>

namespace libharu {
	
class Text;
class Link;
class Rule;
class Image;

class Page {

public:
	explicit Page(const HPDF_Page&);
	Page();
	virtual ~Page();
	
	Text& GetText();
	operator HPDF_Page();
	
	float Width();
	float Height();
	
	void AddLink(const Link& link);
	void AddRule(const Rule& link);
	void AddImage(const Image& link);
	
	void Close();
	
private:

	void DrawRules();
	void DrawLinks();
	void DrawImages();

	HPDF_Page mPage;
	std::list<std::shared_ptr<Link>> mLinks;
	std::list<std::shared_ptr<Rule>> mRules;
	std::list<std::shared_ptr<Image>> mImages;
	
	std::shared_ptr<Text> mText;
};

}

#endif /* defined(__mkpdf__Page__) */
