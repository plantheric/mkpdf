//
//  Font.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Font.h"

namespace libharu {
	
Font::Font(HPDF_Font font) {
	mFont = font;
}

Font::~Font() {
	
}


HPDF_Font Font::Get() const {
	return mFont;
}

}
