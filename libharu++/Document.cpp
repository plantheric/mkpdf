//
//  Document.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Document.h"
#include "Page.h"
#include "Font.h"
#include "Image.h"

#include <string>
#include <iostream>

#include "boost/predef.h"
#include "hpdf.h"

namespace libharu {
	
static void ErrorHandler(HPDF_STATUS error_no, HPDF_STATUS detail_no, void *user_data) {
	Document* document = static_cast<Document*>(user_data);
}

Document::Document() {

	mPdf = HPDF_New (ErrorHandler, this);
//	HPDF_UseUTFEncodings(mPdf);
//	HPDF_SetCurrentEncoder(mPdf, "UTF-8");
	HPDF_SetCompressionMode (mPdf, HPDF_COMP_ALL);
}

Document::Document(const Document& doc) {
	
}

Document::~Document() {

	HPDF_Free(mPdf);
}

bool Document::Save(const std::string &filePath) {

	HPDF_SaveToFile (mPdf, filePath.c_str());
	return true;
}

Page Document::NewPage() {

	Page page (HPDF_AddPage(mPdf));
	mPages.push_back(page);
	
	return page;
}

Image Document::GetImage(const std::vector<uint8_t>& imageData) {
	
	HPDF_Image image = NULL;

	if (!imageData.empty()) {

		image = HPDF_LoadPngImageFromMem (mPdf, &imageData[0], static_cast<HPDF_UINT>(imageData.size()));
	}

	return Image(image);
}

Font Document::GetFont(const std::string& fontPath) {
	
	std::string fontName = mFontNameCache.get(fontPath, [&](const std::string &path) -> std::string {
		
		return HPDF_LoadTTFontFromFile(mPdf, path.c_str(), HPDF_TRUE);
	});
	
	return Font (HPDF_GetFont (mPdf, fontName.c_str(), "MacRomanEncoding"));
}

}
