//
//  Image.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 14/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Image.h"

#include "Document.h"
#include "Page.h"

#include "hpdf.h"

namespace libharu {
	
Image::Image(HPDF_Image& image) : mImage(image) {
	
}

void Image::Draw(Page& page) {

	if (mImage) {
		HPDF_Page_DrawImage (page, mImage, mX, mY, Width(), Height());
	}
}

void Image::Position(float x, float y) {
	
	mX = x;
	mY = y;
}

float Image::Width() const {
	
	return HPDF_Image_GetWidth (mImage);
}

float Image::Height() const {
	
	return HPDF_Image_GetHeight(mImage);
}

}
