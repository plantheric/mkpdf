//
//  Link.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 03/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Link.h"

#include "hpdf.h"

namespace libharu {
	
Link::Link(float left, float right, float top, float bottom, std::string url) :
			left(left), right(right), top(top), bottom(bottom), url(url) {
	
}

HPDF_Rect Link::Rect() {
	
	return HPDF_Rect {left, bottom, right, top};
}

}
