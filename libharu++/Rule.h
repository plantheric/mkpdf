//
//  Rule.h
//  mkpdf
//
//  Created by Mark Bartlett on 17/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Rule__
#define __mkpdf__Rule__

namespace libharu {
	
class Rule {
public:
	Rule(float y, float margin);
	
	float y;
	float margin;
};

}

#endif /* defined(__mkpdf__Rule__) */
