//
//  Link.h
//  mkpdf
//
//  Created by Mark Bartlett on 03/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Link__
#define __mkpdf__Link__

#include <string>
#include "libharufwd.h"

namespace libharu {
	
class Link {
public:
	
	Link(float left, float right, float top, float bottom, std::string url);
	
	HPDF_Rect Rect();
	
	float left;
	float right;
	float top;
	float bottom;
	
	std::string url;
};

}

#endif /* defined(__mkpdf__Link__) */
