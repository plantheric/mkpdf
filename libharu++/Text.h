//
//  Text.h
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Text__
#define __mkpdf__Text__

#include <memory>
#include <string>

#include "libharufwd.h"

namespace libharu {
	
class Page;
class TextState;
class Font;

class Text {

public:
	explicit Text(Page& page);
	Text();

	Text& operator=(const Text& text);

	virtual ~Text();
	
	void SetFont(const Font& family, int size);
	void SetTextLeading(float leading);
	
	long Flow(const std::string &text, const std::string &url,
			  	float x, float y, float width, float height, float firstLineIndent);

	float X();
	float Y();
	float NextLine();

private:

	void AddURL(const std::string& url, float x, float y, float width);

	void SetTextPosition(float x, float y);
	float LineOffset();
	float TextWidth(const std::string &text, long characters = std::string::npos);
	float AdjustWidth(const std::string& text, float oldWidth, float x);
	float mFontBBoxTop;

	Page *mPage;
	std::shared_ptr<TextState> mTextState;
};

}

#endif /* defined(__mkpdf__Text__) */
