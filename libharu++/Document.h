//
//  Docment.h
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef mkpdf_Docment_h
#define mkpdf_Docment_h

#include <list>
#include <string>
#include <vector>

#include "cache.h"

#include "libharufwd.h"

namespace libharu {

class Page;
class Font;
class Image;

class Document {
	
public:
	Document();
	Document(const Document&);
	virtual ~Document();

	bool Save(const std::string &filePath);

	Page NewPage();
	Font GetFont(const std::string& fontPath);
	Image GetImage(const std::vector<uint8_t>& imageData);

private:
	
	std::list<Page> mPages;

	HPDF_Doc  mPdf;
	cache<std::string, std::string> mFontNameCache;
};

}
#endif
