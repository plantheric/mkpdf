//
//  Font.h
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Font__
#define __mkpdf__Font__

#include "libharufwd.h"

namespace libharu {
	
class Font {
	
public:
	Font(HPDF_Font font);
	~Font();
	
	HPDF_Font Get() const;
private:
	HPDF_Font mFont;
};

}

#endif /* defined(__mkpdf__Font__) */
