//
//  Text.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//
#include <iostream>

#include "Text.h"
#include "Page.h"
#include "Font.h"
#include "Link.h"

#include "hpdf.h"

namespace libharu {
	
class TextState {
public:
	TextState(HPDF_Page page);
	~TextState();
private:
	HPDF_Page mPDFPage;
};

Text::Text(Page& page) : mFontBBoxTop(1), mPage(&page) {

	mTextState = std::shared_ptr<TextState>(new TextState(*mPage));
}

Text::Text() : mFontBBoxTop(1), mPage(NULL) {
	
}

Text& Text::operator=(const Text& text)  {

	mPage = text.mPage;
	mTextState = text.mTextState;
	return *this;
}

Text::~Text() {
}

void Text::SetFont(const Font& family, int size) {

	HPDF_Page_SetFontAndSize (*mPage, family.Get(), size);

	HPDF_Box bbox = HPDF_Font_GetBBox (family.Get());
	mFontBBoxTop = (bbox.top)/1000;
}

void Text::SetTextLeading(float leading) {

	HPDF_Page_SetTextLeading (*mPage, leading);
}

long Text::Flow(const std::string &text, const std::string &url, float x, float y, float width, float height, float firstLineIndent) {
	
//	std::cout << text << ": " << TextWidth(text) << "| x " << x << ", y " << y << ", i " << firstLineIndent;
//	std::cout << ", width " << width << ", height " << height << std::endl << std::endl;

	long charactersRemaining = text.length();
	float lineHeight = HPDF_Page_GetTextLeading(*mPage);
	
	while (charactersRemaining > 0 && height > lineHeight) {

		std::string remainingText = text.substr(text.length()-charactersRemaining);
		float newWidth = AdjustWidth(remainingText, width, firstLineIndent);

		unsigned int charactersWritten = 0;
		HPDF_BOOL wrapCanBreakWords = (firstLineIndent > 0) ? HPDF_FALSE : HPDF_TRUE;

		HPDF_Page_TextRect (*mPage, x+firstLineIndent, y, x+newWidth, y-lineHeight,
										remainingText.c_str(), HPDF_TALIGN_LEFT, wrapCanBreakWords, &charactersWritten);

		charactersRemaining -= charactersWritten;

		AddURL(url, x+firstLineIndent, Y(), TextWidth(remainingText, charactersWritten));
		SetTextPosition(X(), Y() + LineOffset());

		y -= lineHeight;
		height -= lineHeight;
		firstLineIndent = 0;
	}

	return charactersRemaining;
}

void Text::AddURL(const std::string& url, float x, float y, float width) {
	
	if (url.empty() || width == 0) {
		return;
	}
	
	Link link(x, x+width, y+LineOffset(), y-2, url);
	mPage->AddLink(link);
}

//	AdjustWidth
//
//	If text starts with punctuation which would immediately wrap to next line adjust the width to fit the punctuation in.
//
//	ie give the markdown "[Title](url)." we don't want the full stop (which would be in a new TextRun) to wrap to the next line
//
float Text::AdjustWidth(const std::string& text, float oldWidth, float x) {

	float newWidth = oldWidth;
	
	for(char c : {'.', ',', ';', ':', '?', '!'}) {
		if (text[0] == c) {
			float textWidth = TextWidth(std::string(1, c));
			if ((x + textWidth) > oldWidth) {
				newWidth = x + (textWidth * 1.1);
				break;
			}
		}
	}

	return newWidth;
}

float Text::NextLine() {
	
	HPDF_Page_MoveToNextLine (*mPage);
	return Y();
}

void Text::SetTextPosition(float x, float y) {
	
	HPDF_Page_SetTextMatrix(*mPage, 1, 0, 0, 1, x, y);
}

float Text::LineOffset() {
	
	return HPDF_Page_GetCurrentFontSize(*mPage) * mFontBBoxTop;
}

float Text::TextWidth(const std::string &text, long characters) {

	return HPDF_Page_TextWidth(*mPage, text.substr(0, characters).c_str());
}

float Text::Y() {
	
	return HPDF_Page_GetCurrentTextPos(*mPage).y;
}

float Text::X() {
	
	return HPDF_Page_GetCurrentTextPos(*mPage).x;
}

TextState::TextState (HPDF_Page page) : mPDFPage(page) {

	HPDF_Page_BeginText(mPDFPage);
}

TextState::~TextState () {

	HPDF_Page_EndText(mPDFPage);
}

}
