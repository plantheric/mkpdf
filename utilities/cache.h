//
//  cache.h
//  mkpdf
//
//  Created by Mark Bartlett on 16/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef mkpdf_cache_h
#define mkpdf_cache_h

#include <functional>
#include <map>

template<typename K, typename V>
class cache {
public:
	V operator[](K key) {
		return get(key, [](K) -> V { throw; return V(); }, [](V){});
	}

	V get(K key, std::function<V (const K&)> miss) {
		return get(key, miss, [&](V){});
	}
	
	V get(K key, std::function<V (const K&)> miss, std::function<void (V)> hit) {
		V val;
		if (mStore.find(key) == mStore.end()) {
			val = miss(key);
			mStore[key] = val;
		} else {
			val = mStore[key];
			hit(val);
		}
		return val;
	}

	void remove(const V& val) {
		
		for (auto i = mStore.begin(); i != mStore.end(); ) {
			if (i->second == val) {
				i = mStore.erase(i);
			} else {
				i++;
			}
		}
	}
private:
	std::map<K, V> mStore;
};


#endif
