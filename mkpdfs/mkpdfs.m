//
//  mkpdfs.m
//  mkpdf
//
//  Created by Mark Bartlett on 05/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "mkpdf.h"

static bool inProgress = false;

@interface MKPDFS : NSObject

@end

@implementation MKPDFS

- (void)convert:(NSPasteboard*)pasteBoard
	   userData:(NSString*)userData
		  error:(NSString**)error {

	inProgress = true;
	
	NSArray* nsurlObjects = [pasteBoard readObjectsForClasses:@[[NSURL class]] options:@{}];

	for (NSURL* sourceFileURL in nsurlObjects) {
	
		if ([[NSFileManager defaultManager] fileExistsAtPath:[sourceFileURL path]]) {
			
			char* destinationFilePath = mkpdfDestinationFilePath([sourceFileURL fileSystemRepresentation]);
			
			bool processFile = true;
			if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:destinationFilePath]]) {

				CFStringRef messsage = CFStringCreateWithFormat (kCFAllocatorDefault, NULL,
															CFSTR("The file '%s' already exists. "
																  "Converting the selected file will overwrite this file. \r"
																  "Continue?"), destinationFilePath);
				CFOptionFlags result;
				CFUserNotificationDisplayAlert(0, kCFUserNotificationStopAlertLevel,
												NULL, NULL, NULL, CFSTR("Convert Markdown to PDF"),
												messsage, CFSTR("OK"), CFSTR("Cancel"), NULL, &result);
				CFRelease(messsage);
				
				processFile = (result == kCFUserNotificationDefaultResponse);
			}
			
			if (processFile) {
				mkpdf([sourceFileURL fileSystemRepresentation], destinationFilePath, false);
			}

			free(destinationFilePath);
		}
	}

	inProgress = false;
}

@end

int main (int argc, char *argv[]) {

	MKPDFS* service = [[MKPDFS alloc] init];
	
	NSRegisterServicesProvider(service, @"mkpdfs");
	NSUpdateDynamicServices();
	
	NSRunLoop *currentLoop = [NSRunLoop currentRunLoop];
	[currentLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:3]];
	
	@try {

		while (inProgress && [currentLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:3]]) {
		}
	}
	@catch (NSException *localException) {
		NSLog(@"Exception %@", localException);
	}
}
