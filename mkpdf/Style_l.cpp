//
//  Style_l.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 08/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Style.h"

#include <fontconfig/fontconfig.h>

std::string Style::GetFontPath(const std::string &familyName, bool bold, bool italic) const {

	std::string name = familyName;

	if (bold) {
		name += ":bold";
	}
	if (italic) {
		name += ":italic";
	}
	
	FcPattern* pattern = FcNameParse((FcChar8*)name.c_str());
	FcConfigSubstitute (0, pattern, FcMatchPattern);
	FcDefaultSubstitute (pattern);
	FcResult result;
	FcPattern* match = FcFontMatch (0, pattern, &result);
	FcPatternDestroy (pattern);
	
	std::string path;
	
	FcChar8* p = FcPatternFormat (match, (const FcChar8*)"%{file}");
	
	if (p) {
		path = (const char*)p;
		FcStrFree(p);
	}
	FcPatternDestroy (match);
	FcFini();

	return path;
}
