//
//  StyleSettings.h
//  mkpdf
//
//  Created by Mark Bartlett on 11/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__StyleSettings__
#define __mkpdf__StyleSettings__

#include <string>
#include <vector>

class StyleSettings {
public:
	StyleSettings();
	virtual ~StyleSettings();

	struct PageMargin {
		float leading;
		float trailing;
		float horizontal;
		float indent;
	};
	
	struct BlockMargin {
		float leading;
		float trailing;
		float left;
	};
	
	struct Font {
		std::string name;
		float size;
		float lineHeight;
	};

	PageMargin mPageMargin;
	BlockMargin mBlockMargin;
	Font mBodyFont;
	Font mCodeFont;
	std::vector<Font> mHeaderFont;
};

class DefaultStyleSettings : public StyleSettings {
public:
	DefaultStyleSettings();
};

#endif /* defined(__mkpdf__StyleSettings__) */
