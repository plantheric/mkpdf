//
//  mkpdf.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 05/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "mkpdf.h"

#include "Document.h"
#include "Markdown.h"
#include "Converter.h"

#include <string>
#include <list>
#include <fstream>
#include <iostream>

std::list<std::string> readFile (const char* filePath);
std::string makeOutputFilePath (const char* filePath);

int mkpdf(const char* sourceFilePath, const char* destinationFilePath, bool printDebug) {
	
	std::string outFilePath = destinationFilePath ? destinationFilePath : makeOutputFilePath(sourceFilePath);
	
	std::list<std::string> lines = readFile(sourceFilePath);
	markdown::Markdown md(lines);
	md.Parse();
	
	if (printDebug) {
		md.DebugPrint();
	}

	auto doc = libharu::Document();
	
	Converter conv(doc);
	conv.Convert(md);
	
	doc.Save(outFilePath);
	
	return EXIT_SUCCESS;
}

char* mkpdfDestinationFilePath(const char* sourceFilePath) {
	
	std::string path = makeOutputFilePath (sourceFilePath);

	return strdup(path.c_str());
}

std::list<std::string> readFile (const char* filePath) {
	
	std::ifstream file (filePath);
	std::string line;
	std::list<std::string> lines;
	
	while (std::getline (file, line)) {
		lines.push_back(line);
	}
	
	return lines;
}

std::string makeOutputFilePath (const char* path) {
	
	std::string filePath = path;
	
	auto lastDot = filePath.find_last_of('.');
	auto lastSlash = filePath.find_last_of('/');
	
	if ((lastDot > lastSlash || lastSlash == std::string::npos) && lastDot != std::string::npos) {
		filePath.erase(lastDot, std::string::npos);
	}
	return filePath + ".pdf";
}

