//
//  LayoutItem.h
//  mkpdf
//
//  Created by Mark Bartlett on 30/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__LayoutItem__
#define __mkpdf__LayoutItem__

#include <memory>

#include "Style.h"

namespace markdown { class Fragment; class Image;}

class LayoutItem {
public:
	LayoutItem(std::shared_ptr<markdown::Fragment>& fragment, const markdown::Block::Style& blockStyle);
	bool IsTextRun();
	bool IsImage();
	
	markdown::TextRun& GetTextRun();
	markdown::Image& GetImage();
	
	Style& GetStyle();
	
	void SetImageSize(float width, float height);
	void CalculateAdjustments(LayoutItem *previous, LayoutItem *next);
	
	float UpdateX(double);
	float UpdateY(double);
	
	bool mFirstFragment;
	bool mLastFragment;
	markdown::Block::Type mBlockType;
	
private:
	std::shared_ptr<markdown::Fragment> mFragment;
	Style mStyle;
	float mXAdj;
	float mYAdj;
	bool mSetX;
	
	float mImageWidth;
	float mImageHeight;
};


#endif /* defined(__mkpdf__LayoutItem__) */
