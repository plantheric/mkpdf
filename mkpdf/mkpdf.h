//
//  mkpdf.h
//  mkpdf
//
//  Created by Mark Bartlett on 05/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__mkpdf__
#define __mkpdf__mkpdf__

#ifdef __cplusplus
extern "C" {
#endif
	
int mkpdf(const char* sourceFilePath, const char* destinationFilePath, bool printDebug);
char* mkpdfDestinationFilePath(const char* sourceFilePath);

#ifdef __cplusplus
}
#endif

#endif /* defined(__mkpdf__mkpdf__) */
