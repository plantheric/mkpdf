//
//  Style.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 31/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Style.h"

#include <map>
#include <algorithm>
#include <array>

#include "cache.h"

#include "Block.h"
#include "TextRun.h"

#include "StyleSettings.h"

using namespace markdown;

std::shared_ptr<StyleSettings> Style::mStyleSettings = std::make_shared<DefaultStyleSettings>();


Style::Style (const Block::Style& blockStyle, const TextRun::Style &textRunStyle) :
				mBlockStyle(blockStyle), mTextRunStyle(textRunStyle) {
	
}

Style::Style (const Block::Style& blockStyle) :
				mBlockStyle(blockStyle) {
	
}

Style::Style () {
	
}

Style::~Style() {
	
}

typedef std::tuple<std::string, bool, bool> fontKey;
static cache<fontKey, std::string> FontPathCache;

std::string Style::FontPath() const {

	bool bold = mTextRunStyle.bold || (mBlockStyle.type == Block::Header);
	bool italic = mTextRunStyle.italic;
	
	StyleSettings::Font &font = Font();

	std::string fontPath = FontPathCache.get(std::make_tuple(font.name, bold, italic), [&](const fontKey &key) -> std::string {
		
		return GetFontPath(std::get<0>(key), std::get<1>(key), std::get<2>(key));
	});

	return fontPath;
}

#if BOOST_OS_MACOS
std::string GetFontPath(const std::string &familyName, bool bold, bool italic);

std::string Style::GetFontPath(const std::string &familyName, bool bold, bool italic) const {

	return ::GetFontPath(familyName, bold, italic);
}
#endif

float Style::FontSize() const {

	StyleSettings::Font &font = Font();

	return font.size;
}

StyleSettings::Font& Style::Font() const {
	
	if (mBlockStyle.type == Block::Header) {
		if (mBlockStyle.number > 0 && mBlockStyle.number <= mStyleSettings->mHeaderFont.size()) {
			return mStyleSettings->mHeaderFont[mBlockStyle.number-1];
		} else {
			return mStyleSettings->mBodyFont;
		}
	}
	else if (mTextRunStyle.mono) {
		return mStyleSettings->mCodeFont;
	}
	else {
		return mStyleSettings->mBodyFont;
	}
}

float Style::LineHeight() const {

	StyleSettings::Font &font = Font();
	
	return font.size * font.lineHeight;
}

float Style::BlockLeadingMargin() const {
	
	return mBlockStyle.breek == Block::ParagraphBreak &&
			mBlockStyle.type == Block::Header				? FontSize() /2  : 0;
}

float Style::BlockTrailingMargin() const {

	return mBlockStyle.breek == Block::ParagraphBreak &&
			mBlockStyle.type != Block::Header				? FontSize() /2  : 0;
}

float Style::PageLeadingMargin() const {

	return mStyleSettings->mPageMargin.leading;
}

float Style::PageTrailingMargin() const {
	
	return mStyleSettings->mPageMargin.trailing;
}

float Style::PageHorizontalMargin() const {
	
	return mStyleSettings->mPageMargin.horizontal;
}

float Style::BlockLeftMargin() const {
	
	return mBlockStyle.type == Block::List ? mStyleSettings->mBlockMargin.left : 0;
}

float Style::BlockIndentMargin() const {
	
	long indentLevel = (mBlockStyle.type == Block::Rule) ? 1 : mBlockStyle.indentLevel;

	return indentLevel * mStyleSettings->mPageMargin.indent;
}


std::string Style::ListBullet() const {
	
	std::string text;
	if (mBlockStyle.number > 0) {
		text = std::to_string(mBlockStyle.number) + ".";
	} else {
		text = "•";
	}

	return text;
}
