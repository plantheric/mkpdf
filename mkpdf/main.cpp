//
//  main.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 24/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "mkpdf.h"

#include <iostream>
#include "boost/program_options.hpp"
namespace po = boost::program_options;

struct parameters {

	std::string inputPath;
	std::string outputPath;
	bool 		debug;
};

static bool parseParameters(int argc, char *argv[], parameters &params);

int main (int argc, char *argv[])
{
	parameters params;

	if (parseParameters(argc, argv, params)) {

		mkpdf (params.inputPath.c_str(),
				(params.outputPath.empty()) ? NULL : params.outputPath.c_str(),
				params.debug);

		return EXIT_SUCCESS;
	}
	else {
		return EXIT_FAILURE;
	}
}

bool parseParameters(int argc, char *argv[], parameters &params) {

	po::options_description desc("Options");
	desc.add_options()
		("help", "produce help message")
		("debug", po::bool_switch(&params.debug)->default_value(false), "enable debug")
		("input-file", po::value<std::string>(&params.inputPath), "markdown file")
		("output-file", po::value<std::string>(&params.outputPath), "pdf file");

	po::positional_options_description pos_desc;
	pos_desc.add("input-file", 1);
	pos_desc.add("output-file", 2);

	
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(desc).positional(pos_desc).run(), vm);
	po::notify(vm);
	
	if (vm.count("help") || vm.count("input-file") == 0) {
		std::cout << desc;
		return false;
	}

	return true;
}

