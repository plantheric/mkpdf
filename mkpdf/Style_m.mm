//
//  Style_m.mm
//  mkpdf
//
//  Created by Mark Bartlett on 08/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include <string>

#import <Cocoa/Cocoa.h>

//	Have to use a free function for this as Style is typedef'd to unsigned char in MacTypes.h

std::string GetFontPath(const std::string &familyName, bool bold, bool italic) {
	
	NSFontTraitMask fontTraitMask = 0;
	
	if (bold) {
		fontTraitMask |= NSBoldFontMask;
	}
	if (italic) {
		fontTraitMask |= NSItalicFontMask;
	}
	
	NSFont* font = [[NSFontManager sharedFontManager] fontWithFamily:[NSString stringWithUTF8String:familyName.c_str()]
															  traits: fontTraitMask
															  weight: 5
																size: 12.0];
	
	CTFontDescriptorRef fontRef = CTFontDescriptorCreateWithNameAndSize ((CFStringRef)[font fontName], [font pointSize]);
	CFURLRef url = (CFURLRef)CTFontDescriptorCopyAttribute(fontRef, kCTFontURLAttribute);
	CFRelease (fontRef);

	NSString *fontPath = [(NSURL *)CFBridgingRelease(url) path];
	std::string path = [fontPath UTF8String];

	return path;
}
