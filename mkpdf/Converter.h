//
//  Converter.h
//  mkpdf
//
//  Created by Mark Bartlett on 03/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Converter__
#define __mkpdf__Converter__

#include "Page.h"
#include "Text.h"

#include "Block.h"

#include "IConv.h"

namespace markdown { class Markdown; }
namespace libharu { class Document; }

class LayoutItem;
class Style;

class Converter {
	
public:
	Converter(libharu::Document& pdfDoc);
	void Convert(const markdown::Markdown& markdown);
	
private:
	
	std::list<LayoutItem> LayoutForBlock(const markdown::Block& block);
	
	void AddPage();

	void AddTextRun(LayoutItem& item, long blockNumber);
	long AddText(const Style &style, const std::string& text, const std::string& url, bool excludeBlockLeftMargin = false);

	void AddImage(LayoutItem& item);
	
	libharu::Page mPage;
	
	float mYPos;
	float mXPos;
	
	IConv mIconv;
	
	libharu::Document& mPDFDoc;
};

#endif /* defined(__mkpdf__Converter__) */
