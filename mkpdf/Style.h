//
//  Style.h
//  mkpdf
//
//  Created by Mark Bartlett on 31/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Style__
#define __mkpdf__Style__

#include <string>

#include "Block.h"
#include "TextRun.h"
#include "StyleSettings.h"

class Style {
public:
	Style (const markdown::Block::Style& blockStyle, const markdown::TextRun::Style & textRunStyle);
	Style (const markdown::Block::Style& blockStyle);
	Style ();
	~Style();

	std::string FontPath() const;
	float FontSize() const;
	float LineHeight() const;
	
	float BlockLeadingMargin() const;
	float BlockTrailingMargin() const;
	float BlockLeftMargin() const;
	std::string ListBullet() const;

	float BlockIndentMargin() const;

	float PageLeadingMargin() const;
	float PageTrailingMargin() const;
	float PageHorizontalMargin() const;

private:
	
	std::string GetFontPath(const std::string &familyName, bool bold, bool italic) const;
	StyleSettings::Font& Font() const;
	
	markdown::Block::Style mBlockStyle;
	markdown::TextRun::Style mTextRunStyle;
	
	static std::shared_ptr<StyleSettings> mStyleSettings;
};

#endif /* defined(__mkpdf__Style__) */
