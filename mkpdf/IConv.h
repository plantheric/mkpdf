//
//  IConv.h
//  mkpdf
//
//  Created by Mark Bartlett on 05/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__IConv__
#define __mkpdf__IConv__

#include <string>

class IConv {

public:
	IConv(const char* destination);
	virtual ~IConv();

	std::string Convert(const std::string &text);

private:
	void* m_iconv;
};

#endif /* defined(__mkpdf__IConv__) */
