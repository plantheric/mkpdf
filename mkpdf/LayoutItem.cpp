//
//  LayoutItem.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 30/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "LayoutItem.h"

#include "markdown/Image.h"

using namespace markdown;

LayoutItem::LayoutItem(std::shared_ptr<Fragment>& fragment, const Block::Style& blockStyle) :
						mFirstFragment(false), mLastFragment(false),
						mFragment(fragment), mStyle(blockStyle),
						mXAdj(0), mYAdj(0), mSetX(false),
						mImageWidth (0), mImageHeight (0) {

	if (IsTextRun()) {
		mStyle = Style(blockStyle, GetTextRun().GetStyle());
	} else if (IsImage()) {
		mStyle = Style(blockStyle);
		GetImage().Download();
	}
	mBlockType = blockStyle.type;
}

bool LayoutItem::IsTextRun() {

	return (typeid(*mFragment) == typeid(TextRun));
}

bool LayoutItem::IsImage() {

	return (typeid(*mFragment) == typeid(markdown::Image));
}

TextRun& LayoutItem::GetTextRun() {

	return static_cast<TextRun&>(*mFragment);
}

markdown::Image& LayoutItem::GetImage() {

	return static_cast<markdown::Image&>(*mFragment);
}

Style& LayoutItem::GetStyle() {

	return mStyle;
}

void LayoutItem::SetImageSize(float width, float height) {

	mImageWidth = width;
	mImageHeight = height;
}

void LayoutItem::CalculateAdjustments(LayoutItem *previous, LayoutItem *next) {

	if (IsImage()) {

		mXAdj = mImageWidth;
		mYAdj = - (mImageHeight - GetStyle().LineHeight());
	}
	else if (IsTextRun()) {

		if (mBlockType == Block::Preformatted && !mLastFragment) {
			mYAdj = - GetStyle().LineHeight();
			mXAdj = GetStyle().PageHorizontalMargin();
			mSetX = true;
		}
	}
}

float LayoutItem::UpdateX(double x) {

	return mSetX ? mXAdj : x + mXAdj;
}

float LayoutItem::UpdateY(double y) {
	
	return y + mYAdj;
}


