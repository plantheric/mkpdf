//
//  Converter.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 03/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Converter.h"

#include "Markdown.h"
#include "Block.h"
#include "TextRun.h"
#include "markdown/Image.h"

#include "Document.h"
#include "Font.h"
#include "Rule.h"
#include "libharu++/Image.h"

#include "Style.h"
#include "LayoutItem.h"

using namespace libharu;
using namespace markdown;


Converter::Converter(Document& pdfDoc) : mYPos(0), mXPos(0), mIconv("MACINTOSH"), mPDFDoc(pdfDoc) {
	
}

void Converter::Convert(const Markdown& markdown) {

	AddPage();
	
	for (auto block : markdown.Blocks()) {
		
		const Block::Style& blockStyle = block->GetStyle();
		Style style(blockStyle);
		mYPos -= style.BlockLeadingMargin();

		if (blockStyle.type == Block::Rule) {
			mPage.AddRule(Rule(mYPos, style.PageHorizontalMargin()+style.BlockIndentMargin()));
		}
		
		std::list<LayoutItem> layout = LayoutForBlock(*block);
		
		for (auto fragment = layout.begin(); fragment != layout.end(); fragment++) {
			
			if (fragment->IsTextRun()) {
				AddTextRun(*fragment, blockStyle.number);
			}
			if (fragment->IsImage()) {
				AddImage(*fragment);
			}
			
			mXPos = fragment->UpdateX(mXPos);
			mYPos = fragment->UpdateY(mYPos);
		}
		mYPos -= style.LineHeight() + style.BlockTrailingMargin();
		mXPos = style.PageHorizontalMargin();
	}

	mPage.Close();
}

std::list<LayoutItem> Converter::LayoutForBlock(const Block& block) {

	std::list<LayoutItem> layout;
	
	for (auto fragment : block.Fragments()) {
		layout.emplace_back(fragment, block.GetStyle());
	}
	
	layout.front().mFirstFragment = true;
	layout.back().mLastFragment = true;

	for (auto& fragment : layout) {
		if (fragment.IsImage()) {

			libharu::Image image = mPDFDoc.GetImage(fragment.GetImage().Data());
			fragment.SetImageSize(image.Width(), image.Height()	);
		}
	}
	
	for (auto fragment = layout.begin(); fragment != layout.end(); fragment++) {
		fragment->CalculateAdjustments(&*fragment, &*fragment);
	}

	return layout;
}

void Converter::AddPage() {

	mPage.Close();

	Style style;

	mPage = mPDFDoc.NewPage();
	mYPos = mPage.Height() - style.PageLeadingMargin();
	mXPos = style.PageHorizontalMargin();
}

void Converter::AddTextRun(LayoutItem& item, long blockNumber) {

	std::string text = mIconv.Convert(item.GetTextRun().Text());
	long remaining = text.length();

	bool addBlockPrefix = item.mFirstFragment && item.mBlockType == Block::List && blockNumber >= 0;
	bool addedPage = false;
	
	while (remaining > 0) {
		
		if (addBlockPrefix) {
			addBlockPrefix = AddText(item.GetStyle(), mIconv.Convert(item.GetStyle().ListBullet()), "", true) != 0;
		}
		
		remaining = AddText(item.GetStyle(), text, item.GetTextRun().URL());
		
		if (remaining <= 0 ||								//	Written all the text or
			(remaining == text.length() && addedPage)) {	//	unable to write any more
			break;
		}
		
		AddPage();
		addedPage = true;
		text = text.substr(text.length() - remaining);
	}

	mXPos = mPage.GetText().X();
	mYPos = mPage.GetText().Y();
}

long Converter::AddText(const Style &style, const std::string& text, const std::string& url, bool excludeBlockLeftMargin) {

	mPage.GetText().SetFont(mPDFDoc.GetFont(style.FontPath()), style.FontSize());
	mPage.GetText().SetTextLeading(style.LineHeight());
	
	float left = style.PageHorizontalMargin() + style.BlockIndentMargin() + (excludeBlockLeftMargin ? 0 : style.BlockLeftMargin());
	float width = mPage.Width() - left - style.PageHorizontalMargin();
	float height = mYPos-style.PageTrailingMargin();
	
	return mPage.GetText().Flow(text, url, left, mYPos, width, height, std::max<float>(mXPos - left, 0));
}

void Converter::AddImage(LayoutItem& item) {

	libharu::Image lhImage = mPDFDoc.GetImage(item.GetImage().Data());

	if (mYPos-item.GetStyle().PageTrailingMargin() < lhImage.Height()) {
		AddPage();
	}
	
	lhImage.Position(mXPos, mYPos-lhImage.Height());
	mPage.AddImage(lhImage);
}
