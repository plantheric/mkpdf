//
//  IConv.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 05/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "IConv.h"

#include <string>
#include <vector>
#include <algorithm>
#include <iconv.h>

IConv::IConv(const char* destination) {

	m_iconv = iconv_open(destination, "UTF-8");
}

std::string IConv::Convert(const std::string &inText) {

	std::vector<char> inData(inText.begin(), inText.end());
	char* inbuf = &inData[0];
	size_t inbytesleft = inData.size();

	std::vector<char> outData(std::min<long>(inText.length(), 100));
	char* outbuf = &outData[0];
	size_t outbytesleft = outData.size();

	std::string outText;

	while (inbytesleft > 0) {
		outbuf = &outData[0];
		outbytesleft = outData.size();
		iconv (m_iconv, &inbuf,  &inbytesleft, &outbuf, &outbytesleft);

		outText += std::string(&outData[0], outData.size() - outbytesleft);
	}

	return outText;
}

IConv::~IConv() {

	iconv_close(m_iconv);
}
