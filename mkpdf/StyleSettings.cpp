//
//  StyleSettings.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 11/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "StyleSettings.h"


StyleSettings::StyleSettings() {
	
}

StyleSettings::~StyleSettings() {
	
}

DefaultStyleSettings::DefaultStyleSettings() {
	
	mPageMargin = StyleSettings::PageMargin { 72, 60, 72, 48 };
	mBlockMargin = StyleSettings::BlockMargin { 6.5, 6.5, 18 };

	mBodyFont = StyleSettings::Font { "Arial", 13, 1.3 };
	mCodeFont = StyleSettings::Font { "Courier New", 13, 1.3 };

	for (int size : {19, 15}) {
		Font headerFont = mBodyFont;
		headerFont.size = size;
		
		mHeaderFont.push_back(headerFont);
	}
}
