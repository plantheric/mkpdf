[mkpdf](https://bitbucket.org/plantheric/mkpdf/) is a tool to convert a simple markdown document to PDF.

mkpdf runs as a command line utility on OS X and Linux, and also includes an OS X System Service.
