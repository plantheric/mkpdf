//
//  ReferenceLink.h
//  mkpdf
//
//  Created by Mark Bartlett on 14/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__ReferenceLink__
#define __mkpdf__ReferenceLink__

#include <string>

namespace markdown {
	
class ReferenceLink {

public:
	ReferenceLink();
	ReferenceLink(const std::string& id, const std::string& url);
	~ReferenceLink();
	
	std::string id;
	std::string url;
};

}

#endif /* defined(__mkpdf__ReferenceLink__) */
