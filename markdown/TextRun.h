//
//  TextRun.h
//  mkpdf
//
//  Created by Mark Bartlett on 25/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__TextRun__
#define __mkpdf__TextRun__

#include <string>

#include "Fragment.h"


namespace markdown {
	
class TextRun : public Fragment {
public:
	TextRun(long startIndex, long endIndex,
			long startContentOffset, long endContentOffset,
			bool italic, bool bold, bool mono,
			const std::string& text, const std::string& url);

	virtual ~TextRun();
	
	bool IsEmpty() const;
	void Append(const std::string &text);
	Fragment* Split(long pos);

	
	void Merge(Fragment&);

	
	std::string Text() const;
	
	struct Style {
		bool italic;
		bool bold;
		bool mono;
	};

	Style GetStyle() const;

	void DebugPrint() const;

private:

	long mStartContentOffset;
	long mEndContentOffset;

	Style mStyle;
	
	std::string mText;
};

}

#endif /* defined(__mkpdf__TextRun__) */
