//
//  Block.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 25/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Block.h"
#include "TextRun.h"
#include "markdown/Image.h"

#include <iostream>
#include <numeric>
#include <iterator>
#include <memory>

#include "boost/regex.hpp"
#include "boost/algorithm/string/replace.hpp"

namespace markdown {
	
typedef boost::match_results<std::string::iterator> match_results;
typedef boost::sub_match<std::string::iterator> sub_match;

static std::string mailto_link(const std::string &address);

Block::Block(std::weak_ptr<Block> previous) {

	Init();
	mPreviousBlock = previous;
}

Block::Block() {

	Init();
}

void Block::Init() {
	
	mStartContentOffset = 0;
	mEndContentOffset = 0;
	
	mStyle.type = Body;
	mStyle.number = 0;
	mStyle.breek = NoBreak;
	mStyle.indentLevel = 0;
}

Block::~Block() {
	
}

void Block::Append(const std::string &text) {

	mRawTextLines.push_back(text);
}

bool Block::IsEmpty() const {

	return mRawTextLines.empty();
}

void Block::SetBreak(Break breek) {

	mStyle.breek = breek;
}

void Block::SetIndentLevel(long indentLevel) {

	mStyle.indentLevel = indentLevel;
}


const Block::Style& Block::GetStyle() const {

	return mStyle;
}


void Block::Parse() {
	
	mRawText = std::accumulate(mRawTextLines.begin(), mRawTextLines.end(), std::string());

	GetBlockType();
	
	//	Generate all the possible TextRuns for this Block
	if (mStyle.type == Block::Preformatted) {
		AddPreformattedTextRuns();
	} else {
		AddFormattingTextRuns();
	}

	mFragments.sort([](std::shared_ptr<Fragment> a, std::shared_ptr<Fragment> b) { return *a < *b; });

	//	Split overlapping TextRuns into separate pieces
	for (auto i = mFragments.begin(); i != mFragments.end(); i++) {

		for (auto j = std::next(i); j != mFragments.end(); j++) {
			
			if ((*i)->StartIndex() <= (*j)->StartIndex() && (*i)->EndIndex() > (*j)->EndIndex()) {
				mFragments.insert(std::next(i), std::shared_ptr<Fragment>((*i)->Split((*j)->EndIndex())));
			}
			if ((*i)->StartIndex() < (*j)->StartIndex() && (*i)->EndIndex() > (*j)->StartIndex()) {
				mFragments.insert(std::next(i), std::shared_ptr<Fragment>((*i)->Split((*j)->StartIndex())));
			}
			if ((*j)->StartIndex() <= (*i)->StartIndex() && (*j)->EndIndex() > (*i)->EndIndex()) {
				mFragments.insert(std::next(i), std::shared_ptr<Fragment>((*j)->Split((*i)->EndIndex())));
			}
			if ((*j)->StartIndex() < (*i)->StartIndex() && (*j)->EndIndex() > (*i)->StartIndex()) {
				mFragments.insert(std::next(i), std::shared_ptr<Fragment>((*j)->Split((*i)->StartIndex())));
			}
		}
	}

	//	Merge TextRuns that cover the same range and aggregate their properties
	for (auto i = mFragments.begin(); i != mFragments.end(); i++) {
		
		for (auto j = std::next(i); j != mFragments.end(); ) {
			
			auto& a = *i->get();
			auto& b = *j->get();

			if (a.Matches(b)) {
				if (typeid(b)== typeid(Image)) {
					b.Merge(a);
					j++;
					i = mFragments.erase(i);
					
				} else {
					a.Merge(b);
					j = mFragments.erase(j);
				}
			}
			else {
				j++;
			}
		}
	}

	//	Remove any empty TextRuns
	for (auto i = mFragments.begin(); i != mFragments.end(); ) {
		if ((*i)->IsEmpty()) {
			i = mFragments.erase(i);
		} else {
			i++;
		}
	}

	mFragments.sort([](std::shared_ptr<Fragment> a, std::shared_ptr<Fragment> b) { return *a < *b; });
}

//	UpdateListItemNumber
//
//	Ordered list items are number sequentially from 1 regardless of the numbers assigned in to source
//
void Block::UpdateListItemNumber(std::list<int>& listItemNumber) {

	if (mStyle.type == List) {
		
		std::shared_ptr<Block> previous = GetPrevious();

		if (previous) {
			if (mStyle.indentLevel > previous->mStyle.indentLevel) {
				listItemNumber.push_back(1);
			}
			if (mStyle.indentLevel < previous->mStyle.indentLevel) {
				listItemNumber.pop_back();
			}
		}
		
		if (mStyle.number > 0) {
			mStyle.number = listItemNumber.back();
			listItemNumber.back()++;
		}
	}
	else {	// Reset the numbering at the end of the list

		listItemNumber.back() = 1;
	}
}

void Block::UpdateListItemNesting(std::list<long>& nestedListIndentLevels) {
	
	//	Calculate indent levels for (nested) list items
	std::shared_ptr<Block> previous = GetPrevious();
	bool previousIsList = previous && previous->mStyle.type == Block::List;
	
	if (previousIsList && mStyle.type == List) {
		
		if (mStartContentOffset > previous->mStartContentOffset) {
			nestedListIndentLevels.push_back(mStartContentOffset);
		}
		if (mStartContentOffset < previous->mStartContentOffset) {
			
			while (nestedListIndentLevels.back() > mStartContentOffset) {
				nestedListIndentLevels.pop_back();
			}
		}
		mStyle.indentLevel += nestedListIndentLevels.size() - 1;
	}
}

const FragmentList& Block::Fragments() const {

	return mFragments;
}

std::shared_ptr<Block> Block::GetPrevious() {
 
	return mPreviousBlock.lock();
}

std::string Block::StealLastLine() {

	std::string text = mRawTextLines.back();
	mRawTextLines.pop_back();

	return text;
}

long Block::RawTextIndex(const std::string::iterator &iter) {

	return std::distance(mRawText.begin(), iter);
}

void Block::Find(const std::string& regex, std::function<void (match_results&)> create) {

	match_results results;
	auto start = next(mRawText.begin(), mStartContentOffset);
	auto end = prev(mRawText.end(), mEndContentOffset);

	for ( ; boost::regex_search(start, end, results, boost::regex(regex));
		 start = results[0].second) {
		
		create(results);
	}
}

void Block::GetBlockType() {

	boost::smatch matches;
	
	std::shared_ptr<Block> previous = GetPrevious();
	bool previousIsList = previous && previous->mStyle.type == Block::List;
	
	if (boost::regex_match(mRawText, matches, boost::regex(R"(^#+\s*([^#]+)\s*#*$)"))) {	//	Header - atx style
		
		mStyle.type = Header;
		mStartContentOffset = std::distance(mRawText.cbegin(), matches[1].first);
		mEndContentOffset = std::distance(matches[1].second, mRawText.cend());
		mStyle.number = std::count(mRawText.cbegin(), matches[1].first, '#');
	}
	else if (mRawTextLines.size() == 2 &&
			 boost::regex_match(mRawTextLines.back(), matches, boost::regex(R"(^[=-]+$)")) &&
			 boost::regex_match(mRawText, matches, boost::regex(R"(.*?([=-]+)$)"))) {		//	Header - setext style
		
		mStyle.type = Header;
		mEndContentOffset = std::distance(matches[1].first, mRawText.cend());;
		mStyle.number = mRawTextLines.back()[0] == '=' ? 1 : 2;
	}
	else if (boost::regex_match(mRawText, matches, boost::regex(R"(^([-\s]{3,}|[_\s]{3,}|[\*\s]{3,})$)"))) {	//	Horizontal rule
		
		mStyle.type = Rule;
		mEndContentOffset = mRawText.length();
	}
	else if (boost::regex_match(mRawText, matches, boost::regex(R"(^[\+\*-]\s+(\S.*))")) ||
			(previousIsList && boost::regex_match(mRawText, matches, boost::regex(R"(^\s+[\+\*-]\s+(\S.*))")))) {	//	Unordered list
		
		mStyle.type = List;
		mStyle.number = 0;
		mStartContentOffset = std::distance(mRawText.cbegin(), matches[1].first);
	}
	else if (boost::regex_match(mRawText, matches, boost::regex(R"(^(\d+)\.\s+(\S.*))")) ||
			(previousIsList && boost::regex_match(mRawText, matches, boost::regex(R"(^\s+(\d+)\.\s+(\S.*))")))) {	// Ordered list
		
		mStyle.type = List;
		mStyle.number = std::stoi(matches[1].str());
		mStartContentOffset = std::distance(mRawText.cbegin(), matches[2].first);
	}
	else if (previousIsList && boost::regex_match(mRawText, matches, boost::regex(R"((\s+)(.*))"))) {		//	Paragraph in list
		
		mStyle.type = List;
		mStyle.number = -1;
		mStartContentOffset = std::distance(mRawText.cbegin(), matches[2].first);
	}
	else if (boost::regex_match(mRawText, matches, boost::regex(R"((\t|[ ]{4}).*)"))) {	// Preformatted
		
		mStyle.type = Preformatted;
	}
}

void Block::AddFormattingTextRuns() {

	AddTextRun(0, mRawText.length(), mStartContentOffset, mEndContentOffset, false, false, false, mRawText);

	//	To handle escaped characters we just erase them. They cannot then trigger any of the markup we search for below.
	//	Because of the way the TextRuns are merged, all of the actual content comes from the first TextRun added above.
	for (const char c : {'\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '#', '>', '+', '-', '.', '!'}) {
		
		std::string find = "\\" + std::string(1, c);
		boost::replace_all(mRawText, find, "xx");
	}

	//	Find code
	Find(R"(.*?([`]{2}.+?[`]{2}).*?)", [&](match_results& results) {
		
		const sub_match& mono = results[1];
		AddTextRun(RawTextIndex(mono.first), RawTextIndex(mono.second), 2, 2, false, false, true, mono.str());
		
		mRawText.replace(mono.first, mono.second, mono.str().length(), ' ');
	});
	
	//	Find code
	Find(R"(.*?([`]{1}.+?[`]{1}).*?)", [&](match_results& results) {
		
		const sub_match& mono = results[1];
		AddTextRun(RawTextIndex(mono.first), RawTextIndex(mono.second), 1, 1, false, false, true, mono.str());
		
		mRawText.replace(mono.first, mono.second, mono.str().length(), ' ');
	});
	
	//	Find images
	Find(R"(.*?(!\[.*?\])(\(.*?\)).*?)", [&](match_results& results) {
		
		const sub_match& alt = results[1];
		const sub_match& url = results[2];

		mFragments.push_back(std::shared_ptr<Image>(new Image(RawTextIndex(alt.first), RawTextIndex(url.second), url.str())));
		mRawText.replace(url.first, url.second, std::distance(url.first, url.second), ' ');
	});
	
	//	Find reference style images
	Find(R"(.*?(!\[.*?\])(\[.*?\]).*?)", [&](match_results& results) {
		
		const sub_match& alt = results[1];
		const sub_match& urlId = results[2];
		
		mFragments.push_back(std::shared_ptr<Image>(new Image(RawTextIndex(alt.first), RawTextIndex(urlId.second), urlId.str())));
		mRawText.replace(urlId.first, urlId.second, std::distance(urlId.first, urlId.second), ' ');
	});
	
	//	Find links
	Find(R"(.*?(\[.*?\])(\(.*?\)).*?)", [&](match_results& results) {
		
		const sub_match& label = results[1];
		const sub_match& url = results[2];
		AddTextRun(RawTextIndex(label.first), RawTextIndex(url.second), 1, 1+url.str().length(), false, false, false, label.str()+url.str(), url.str());
		mRawText.replace(url.first, url.second, std::distance(url.first, url.second), ' ');
	});
	
	//	Find reference style links
	Find(R"(.*?(\[.*?\])(\[.*?\]).*?)", [&](match_results& results) {
		
		const sub_match& label = results[1];
		const sub_match& urlId = results[2];

		AddTextRun(RawTextIndex(label.first), RawTextIndex(urlId.second), 1, 1+urlId.str().length(), false, false, false, label.str()+urlId.str(), urlId.str());
		mRawText.replace(urlId.first, urlId.second, std::distance(urlId.first, urlId.second), ' ');
	});
	
	//	Find email
	Find(R"(.*?(<.+@.+\..+>).*?)", [&](match_results& results) {
		
		const sub_match& email = results[1];
		AddTextRun(RawTextIndex(email.first), RawTextIndex(email.second), 1, 1, false, false, false, email.str(), mailto_link(email.str()));
		
		mRawText.replace(email.first, email.second, std::distance(email.first, email.second), ' ');
	});
	
	//	Find auto links
	Find(R"(.*?(<.+?>).*?)", [&](match_results& results) {
		
		const sub_match& link = results[1];
		AddTextRun(RawTextIndex(link.first), RawTextIndex(link.second), 1, 1, false, false, false, link.str(), link.str());
		
		mRawText.replace(link.first, link.second, std::distance(link.first, link.second), ' ');
	});
	
	//	Find bold
	Find(R"(.*?([_\*]{2}(\S|\S.*?\S)[\*_]{2}).*?)", [&](match_results& results) {
		
		const sub_match& bold = results[1];
		AddTextRun(RawTextIndex(bold.first), RawTextIndex(bold.second), 2, 2, false, true, false, bold.str());
		
		mRawText.replace(bold.first, bold.first+2, 2, ' ');
		mRawText.replace(bold.second-2, bold.second, 2, ' ');
	});
	
	//	Find italics
	Find(R"((^|.*?[^_\*])([_\*]{1}[^_\* ].*?[\*_]{1})([^_\*].*?|$))", [&](match_results& results) {
		
		const sub_match& italic = results[2];
		AddTextRun(RawTextIndex(italic.first), RawTextIndex(italic.second), 1, 1, true, false, false, italic.str());
		
		mRawText.replace(italic.first, italic.first+1, 1, ' ');
		mRawText.replace(italic.second-1, italic.second, 1, ' ');
	});
}

void Block::AddPreformattedTextRuns() {
	
	long startIndex = 0;
	long endIndex = 0;
	for (auto line : mRawTextLines) {
		boost::smatch matches;
		if (boost::regex_match(line, matches, boost::regex(R"((\t|[ ]{4}).*)"))) {	// Preformatted
			endIndex = startIndex + line.length();
			long startContentOffset = *(matches[1].first) == '\t' ? 1 : 4;
			AddTextRun(startIndex, endIndex, startContentOffset, 0, false, false, true, line);
			startIndex = endIndex;
		}
	}
}


void Block::AddTextRun (long startIndex, long endIndex, long startContentOffset, long endContentOffset,
								bool italic, bool bold, bool mono, const std::string& text, const std::string& url) {

	mFragments.push_back(std::shared_ptr<TextRun>(new TextRun(startIndex, endIndex, startContentOffset, endContentOffset, italic, bold, mono, text, url)));
}

void Block::DebugPrint() const {
	
	std::cout << "******" << std::endl;
 
	std::cout << ((mStyle.breek == LineBreak) ? "-" : "¬");

	switch (mStyle.type) {
		case Header:		std::cout << "H"; break;
		case Body:			std::cout << "B"; break;
		case List:			std::cout << "L"; break;
		case Preformatted:	std::cout << "P"; break;
		case Rule:			std::cout << "H"; break;
	}

	for (auto runs : mFragments) {
		runs->DebugPrint();
	}
}

static std::string mailto_link(const std::string &address) {
	
	return "<mailto:"+address.substr(1);
}

}
