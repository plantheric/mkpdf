//
//  Image.h
//  mkpdf
//
//  Created by Mark Bartlett on 12/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__markdown_Image__
#define __mkpdf__markdown_Image__

#include "Fragment.h"

#include "cache.h"
#include <vector>
#include <memory>

namespace markdown {
	
class ImageData;
	
class Image : public Fragment {
	
public:
	Image(long startIndex, long endIndex, const std::string& url);
	~Image();

	void Download();
	void Merge(Fragment& obj);
	virtual bool UpdateReferenceURL(const ReferenceLink &link);

	void DebugPrint() const;
	
	const std::vector<uint8_t>& Data();

private:

	static cache<std::string, std::shared_ptr<ImageData>> mDownloadCache;
};

}

#endif /* defined(__mkpdf__markdown_Image__) */
