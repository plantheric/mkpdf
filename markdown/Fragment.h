//
//  Fragment.h
//  mkpdf
//
//  Created by Mark Bartlett on 12/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__ContentObject__
#define __mkpdf__ContentObject__

#include <string>

namespace markdown {

class ReferenceLink;

class Fragment {

public:
	Fragment(long startIndex, long endIndex, const std::string& url);
	virtual ~Fragment();
	
	bool operator< (const Fragment& run) const;
	
	long StartIndex();
	long EndIndex();

	virtual bool IsEmpty() const;
	bool Matches(const Fragment&);
	virtual Fragment* Split(long pos);
	virtual void Merge(Fragment&);

	virtual bool UpdateReferenceURL(const ReferenceLink &link);
	std::string URL() const;

	virtual void DebugPrint() const;

protected:
	
	long mStartIndex;
	long mEndIndex;

	std::string mURL;
};

}

#endif /* defined(__mkpdf__ContentObject__) */
