//
//  Fragment.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 12/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Fragment.h"

#include "boost/regex.hpp"

#include "ReferenceLink.h"


namespace markdown {
	
Fragment::Fragment (long startIndex, long endIndex, const std::string& url) :
					mStartIndex(startIndex), mEndIndex(endIndex), mURL(url)  {
}

Fragment::~Fragment() {
	
}

bool Fragment::operator< (const Fragment& run) const {
	
	return mStartIndex < run.mStartIndex;
}

long Fragment::StartIndex() {
	
	return mStartIndex;
}

long Fragment::EndIndex() {
	
	return mEndIndex;
}

bool Fragment::IsEmpty() const {

	return mURL.empty();
}

bool Fragment::Matches(const Fragment& run) {
	
	return run.mStartIndex == mStartIndex && run.mEndIndex == mEndIndex;
}

Fragment* Fragment::Split(long pos) {
	
	assert(false);
	return NULL;
}

void Fragment::Merge(Fragment&) {

	assert(false);
}

bool Fragment::UpdateReferenceURL(const ReferenceLink &link) {
	
	if (mURL == link.id) {
		mURL = "("+link.url+")";
		return true;
	}
	
	return false;
}

void Fragment::DebugPrint() const {
	
}

std::string Fragment::URL() const {
	
	std::string url;
	
	boost::smatch matches;
	
	if (!mURL.empty()) {
		
		if (boost::regex_match(mURL, matches, boost::regex(R"(\(([^"\s]*)[ ]*(".*")?\))")))  {
			url = matches[1].str();
		}
		else if (boost::regex_match(mURL, matches, boost::regex(R"(<(.+)>)")))  {
			url = matches[1].str();
		}
	}

	return url;
}

}
