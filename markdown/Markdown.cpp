//
//  Markdown.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 25/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Markdown.h"
#include "Block.h"
#include "TextRun.h"
#include "ReferenceLink.h"

#include "boost/regex.hpp"

#include <algorithm>

namespace markdown {
	
static void IsNewBlock(const std::string& line, bool& startsBlock, bool& endsBlock, bool &stealLastLine,
						Block::Break& breakType, const std::shared_ptr<Block>& blockStyle);

static std::shared_ptr<Block> MakeNewBlockStyle (std::list<std::shared_ptr<Block>> &blockStyle,
													std::shared_ptr<Block>& previous,
													Block::Break breakType,
													long indentLevel);

static bool GetQuoteLevelAndTrimText(long& indentLevel, std::string &text);


Markdown::Markdown(const std::list<std::string> &document) : mDocument(document) {
	
}

Markdown::~Markdown() {
	
}

void Markdown::Parse () {

	auto blockStyle = std::make_shared<Block>();
	
	for (auto line : mDocument) {
		
		if (GetReferenceLink(line)) {
			continue;
		}

		long indentLevel = 0;

		if (GetQuoteLevelAndTrimText(indentLevel, line)) {
			blockStyle->SetIndentLevel(indentLevel);
		}

		bool startsBlock = false;
		bool endsBlock = false;
		bool stealLastLine = false;

		Block::Break breakType = Block::NoBreak;
		
		IsNewBlock(line, startsBlock, endsBlock, stealLastLine, breakType, blockStyle);

		if (startsBlock) {
			std::string text;
			if (stealLastLine) {
				text = blockStyle->StealLastLine();
			}
			blockStyle = MakeNewBlockStyle(mBlocks, blockStyle, breakType, indentLevel);
			if (stealLastLine) {
				blockStyle->Append(text);
			}
		}

		if (AddLine(line)) {
			blockStyle->Append(line);
		}

		if (endsBlock) {
			blockStyle = MakeNewBlockStyle(mBlocks, blockStyle, breakType, indentLevel);
		}
	}

	MakeNewBlockStyle(mBlocks, blockStyle, Block::NoBreak, 0);
	
	//	Correctly nest list items and number orderedone
	std::list<long> nestedListIndentLevels(1, 0);
	std::list<int> listItemNumber(1, 1);

	for (auto block : mBlocks) {
		block->UpdateListItemNesting(nestedListIndentLevels);
		block->UpdateListItemNumber(listItemNumber);
	}

	//	Update any reference style links with the urls found by GetReferenceLink()
	for (auto block : mBlocks) {
		for (auto fragment : block->Fragments()) {
			for (auto link : mReferenceLinks) {
				fragment->UpdateReferenceURL(link);
			}
		}
	}
}

const BlockList& Markdown::Blocks() const {
	
	return mBlocks;
}

void IsNewBlock(const std::string& line, bool& startsBlock, bool& endsBlock, bool& stealLastLine,
				Block::Break& breakType, const std::shared_ptr<Block>& blockStyle) {

	auto previous = blockStyle->GetPrevious();
	
	if (line.empty()) {											//	Blank line
		startsBlock = true;
		breakType = Block::ParagraphBreak;
	}
	else if (line[0] == '#') {										//	Headers - atx style
		startsBlock = true;
		endsBlock = true;
		breakType = Block::ParagraphBreak;
	}
	else if (boost::regex_search(line, boost::regex("(^[=-]+$)")) && !blockStyle->IsEmpty()) {	//	Headers - setext style
		startsBlock = true;
		endsBlock = true;
		stealLastLine = true;
		breakType = Block::ParagraphBreak;
	}
	else if (boost::regex_search(line, boost::regex(R"(^([-\s]{3,}|[_\s]{3,}|[\*\s]{3,})$)")) && blockStyle->IsEmpty()) {	//	Horizontal rule
		startsBlock = true;
		endsBlock = true;
		breakType = Block::ParagraphBreak;
	}
	else if (boost::regex_search(line, boost::regex(R"(^\s{0,3}(\d+\.|[\+\*-])\s+\S)"))) {	//	List items
		startsBlock = true;
		breakType = Block::LineBreak;
	}
	else if (previous && previous->GetStyle().type == Block::List &&
		boost::regex_search(line, boost::regex(R"(^\s*(\d+\.|[\+\*-])\s+\S)"))) {	//	List items
		startsBlock = true;
		breakType = Block::LineBreak;
	}
	else if (boost::regex_search(line, boost::regex("[ ]{2,}$"))) {	//	Manual line break
		endsBlock = true;
		breakType = Block::LineBreak;
	}
}

std::shared_ptr<Block> MakeNewBlockStyle (std::list<std::shared_ptr<Block>> &blockStyles,
											 std::shared_ptr<Block>& block,
											 Block::Break breakType,
											 long indentLevel) {
	if (!block->IsEmpty()) {
		block->SetBreak(breakType);
		block->Parse();
		blockStyles.push_back(block);
		block = std::make_shared<Block>(std::weak_ptr<Block>(block));
	}
	block->SetBreak(breakType);
	block->SetIndentLevel(indentLevel);
	
	return block;
}

bool Markdown::AddLine(const std::string& line) {
	
	return !line.empty();
}

bool Markdown::GetReferenceLink(const std::string& line) {
	
	boost::smatch matches;
	if (boost::regex_match(line, matches, boost::regex(R"(\s*?(\[.*?\]):\s*(.*?)\s*$)"))) {
		mReferenceLinks.push_back(ReferenceLink(matches[1], matches[2]));
		return true;
	}
	return false;
}

void Markdown::DebugPrint() const {
	
	for (auto block : mBlocks ) {
		
		block->DebugPrint();
	}
}

bool GetQuoteLevelAndTrimText(long& indentLevel, std::string &text) {
	
	indentLevel = 0;
	
	boost::smatch matches;
	if (boost::regex_match(text, matches, boost::regex(R"(^ *>[> ]*(.*))"))) {
		indentLevel = std::count(text.cbegin(), matches[1].first, '>');
		text = matches[1];
	}
	return indentLevel > 0;
}

}
