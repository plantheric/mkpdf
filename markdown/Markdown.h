//
//  Markdown.h
//  mkpdf
//
//  Created by Mark Bartlett on 25/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Markdown__
#define __mkpdf__Markdown__

#include <list>
#include <string>
#include <memory>

namespace markdown {
	
class Block;
typedef std::list<std::shared_ptr<Block>> BlockList;
class ReferenceLink;

class Markdown {

public:
	Markdown(const std::list<std::string> &document);
	virtual ~Markdown();
	
	void Parse();
	void DebugPrint() const;

	const BlockList& Blocks() const;
	
private:
	
	bool AddLine(const std::string&);
	bool GetReferenceLink(const std::string& line);

	std::list<std::string> mDocument;
	BlockList mBlocks;
	std::list<ReferenceLink> mReferenceLinks;
};

}

#endif /* defined(__mkpdf__Markdown__) */
