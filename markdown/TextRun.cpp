//
//  TextRun.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 25/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "TextRun.h"

#include "boost/regex.hpp"
#include "boost/algorithm/string/replace.hpp"

#include <iostream>
#include <cassert>

namespace markdown {
	
TextRun::TextRun(long startIndex, long endIndex,
				 long startContentOffset, long endContentOffset,
				 bool italic, bool bold, bool mono,
				 const std::string& text, const std::string& url) :
					Fragment(startIndex, endIndex, url),
					mStartContentOffset(startContentOffset), mEndContentOffset(endContentOffset),
					mText(text) {
	mStyle.italic = italic;
	mStyle.bold = bold;
	mStyle.mono = mono;
}

TextRun::~TextRun() {
	
}

bool TextRun::IsEmpty() const {

	return Text().empty();
}

void TextRun::Append(const std::string &text) {
	
	mText += text;
}

Fragment* TextRun::Split(long pos) {
	
	TextRun* after = new TextRun(*this);
	
	this->mEndIndex = pos;
	this->mEndContentOffset = 0;
	this->mText = after->mText.substr(0, pos-mStartIndex);

	after->mStartIndex = pos;
	after->mStartContentOffset = 0;
	after->mText = after->mText.substr(pos-mStartIndex);
	return after;
}


void TextRun::Merge(Fragment& obj) {
	
	TextRun &run = static_cast<TextRun&>(obj);
	assert(run.mStartIndex == mStartIndex);
	assert(run.mEndIndex == mEndIndex);

	mStyle.italic |= run.mStyle.italic;
	mStyle.bold |= run.mStyle.bold;
	mStyle.mono |= run.mStyle.mono;
	
	mStartContentOffset = std::max(mStartContentOffset, run.mStartContentOffset);
	mEndContentOffset = std::max(mEndContentOffset, run.mEndContentOffset);
	
	mURL = std::max(mURL, run.mURL);
}

std::string TextRun::Text() const {

	std::string text =  mText.substr(mStartContentOffset, mText.length()-(mStartContentOffset+mEndContentOffset));

	if (mStyle.mono) {
		//	Substitute tabs for the appropiate number of spaces
		std::string::size_type position;
		while ((position = text.find_first_of('\t')) != std::string::npos) {

			const int tabSize = 4;
			int spaces = tabSize - (position % tabSize);
			text = text.replace(position, 1, spaces, ' ');
		}
	}
	else {
		//	Unescape any escaped characters
		for (const char c : {'\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '#', '>', '+', '-', '.', '!'}) {

			std::string replace = std::string(1, c);
			std::string find = "\\" + replace;
			boost::replace_all(text, find, replace);
		}
	}
	return text;
}

TextRun::Style TextRun::GetStyle() const {

	return mStyle;
}

void TextRun::DebugPrint() const {
	
	std::cout << "$" << mStartIndex << ":" << mEndIndex << ":";
	if (mStyle.bold)
		std::cout << "b";
	if (mStyle.italic)
		std::cout << "i";
	if (mStyle.mono)
		std::cout << "m";
	if (!mURL.empty())
		std::cout << "u";
	
	std::cout << ":" << Text();

	if (!mURL.empty())
		std::cout << "(" << URL() << ")";
 
	std::cout << "$" << std::endl;
}

}
