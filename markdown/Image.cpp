//
//  Image.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 12/12/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "Image.h"

#include <iostream>
#include <cassert>
#include <future>
#include <thread>

#include <curl/curl.h>


namespace markdown {
	
class ImageData {
public:
	ImageData (const std::string& url);
	
	const std::vector<uint8_t>& Data() { return *mData.get(); }
	
private:
	std::shared_future<std::shared_ptr<std::vector<uint8_t>>> mData;
	
	std::shared_ptr<std::vector<uint8_t>> mTempData;
	static size_t CurlCallback(void *contents, size_t size, size_t nmemb, void *user);
};
	
cache<std::string, std::shared_ptr<ImageData>> Image::mDownloadCache;

	
Image::Image (long startIndex, long endIndex, const std::string& url) :
				Fragment(startIndex, endIndex, url)  {

	Download();
}

Image::~Image() {
	
}

void Image::Merge(Fragment& obj) {

}

bool Image::UpdateReferenceURL(const ReferenceLink &link) {
	
	bool updated = Fragment::UpdateReferenceURL(link);
	if (updated) {
		Download();
	}
	return updated;
}

void Image::DebugPrint() const {

	std::cout << "$" << mStartIndex << ":" << mEndIndex << ":";
	
	if (!mURL.empty())
		std::cout << "Image(" << URL() << ")";
 
	std::cout << "$" << std::endl;
}

const std::vector<uint8_t>& Image::Data() {

	return mDownloadCache[URL()]->Data();
}

void Image::Download() {
	
	std::string url = URL();
	
	if (url.empty()) {
		return;
	}
	
	mDownloadCache.get(url, [](const std::string &url) -> std::shared_ptr<ImageData> {

		return std::make_shared<ImageData>(url);
	});
}

ImageData::ImageData (const std::string& url) {
	
	mData = std::async(std::launch::async, [this](const std::string &url) {
		
		CURL* curl = curl_easy_init();
		if (curl) {
			
			mTempData = std::make_shared<std::vector<uint8_t>>();
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ImageData::CurlCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
			
			CURLcode res = curl_easy_perform(curl);
			if (res != CURLE_OK) {
				std::cout << "curl_easy_perform() failed: " << curl_easy_strerror(res) << " URL: "<< url << std::endl;
			}
			
			curl_easy_cleanup(curl);
		}
		
		return mTempData;
	}, url);
}

size_t ImageData::CurlCallback(void *contents, size_t size, size_t nmemb, void *user) {
	
	auto image = static_cast<ImageData*>(user);
	size_t realSize = size * nmemb;
	
	auto buffer = static_cast<uint8_t*>(contents);
	image->mTempData->insert(image->mTempData->end(), buffer, buffer+realSize);
	
	return realSize;
}

}
