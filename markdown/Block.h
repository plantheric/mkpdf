//
//  Block.h
//  mkpdf
//
//  Created by Mark Bartlett on 25/10/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#ifndef __mkpdf__Block__
#define __mkpdf__Block__

#include <list>
#include <string>
#include <memory>
#include <functional>
#include "boost/regex.hpp"

namespace markdown {
	
class Fragment;
typedef std::list<std::shared_ptr<Fragment>> FragmentList;

class Block {

public:
	Block();
	Block(std::weak_ptr<Block> previous);
	virtual ~Block();
	
	void Append(const std::string &text);
	bool IsEmpty() const;
	void Parse();

	void UpdateListItemNesting(std::list<long>& nestedListIndentLevels);
	void UpdateListItemNumber(std::list<int>& listItemNumber);

	enum Break { NoBreak, LineBreak, ParagraphBreak };
	enum Type { Header, List, Body, Preformatted, Rule };

	void SetBreak(Break breek);
	void SetIndentLevel(long indentLevel);

	struct Style {

		Break breek;
		Type type;
		long number;
		long indentLevel;
	};
	
	const Style& GetStyle() const;
	
	const FragmentList& Fragments() const;

	std::string StealLastLine();
	
	std::shared_ptr<Block> GetPrevious();

	void DebugPrint() const;
	
private:

	void Init();
	void Find(const std::string& regex, std::function<void (boost::match_results<std::string::iterator>&)> create);
	void GetBlockType();
	void AddFormattingTextRuns();
	void AddPreformattedTextRuns();
	void AddTextRun (long startIndex, long endIndex, long startContentOffset, long endContentOffset,
						 bool italic, bool bold, bool mono, const std::string& text, const std::string& url="");

	long RawTextIndex(const std::string::iterator &iter);

	Style mStyle;
	FragmentList mFragments;
	std::list<std::string> mRawTextLines;
	std::string mRawText;
	long mStartContentOffset;
	long mEndContentOffset;
	
	std::weak_ptr<Block> mPreviousBlock;
};

}

#endif /* defined(__mkpdf__Block__) */
