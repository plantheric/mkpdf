//
//  ReferenceLink.cpp
//  mkpdf
//
//  Created by Mark Bartlett on 14/11/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#include "ReferenceLink.h"

namespace markdown {
	
ReferenceLink::ReferenceLink() {
}

ReferenceLink::ReferenceLink(const std::string& id, const std::string& url) :
							id(id), url(url) {
}

ReferenceLink::~ReferenceLink() {
}

}