vpath %.cpp mkpdf libharu++ markdown boost_1_56_0/libs/regex/src boost_1_56_0/libs/program_options/src

CC=g++
INC_DIR=. libharu++ libharu/lib/include markdown boost_1_56_0 utilities
CFLAGS=-c -Wall $(INC_DIR:%=-I%) -std=c++0x -O2
LDFLAGS=-L./libharu/lib/lib -L/usr/lib/i386-linux-gnu/
LIBS=libharu/lib/lib/libhpdf.a -lfontconfig -lcurl -lpng -lz -lpthread
EXECUTABLE=mkpdf
BIN_DIR=build

SOURCES=main.cpp mkpdf.cpp Style.cpp Style_l.cpp StyleSettings.cpp Converter.cpp LayoutItem.cpp IConv.cpp \
	Document.cpp Page.cpp libharu++/Image.cpp Text.cpp Font.cpp Link.cpp Rule.cpp \
	Markdown.cpp Fragment.cpp markdown/Image.cpp Block.cpp TextRun.cpp ReferenceLink.cpp \
	c_regex_traits.cpp cpp_regex_traits.cpp cregex.cpp fileiter.cpp icu.cpp instances.cpp posix_api.cpp regex_debug.cpp regex_raw_buffer.cpp regex_traits_defaults.cpp regex.cpp static_mutex.cpp usinstances.cpp w32_regex_traits.cpp wc_regex_traits.cpp wide_posix_api.cpp winstances.cpp \
	cmdline.cpp config_file.cpp convert.cpp options_description.cpp parsers.cpp positional_options.cpp split.cpp utf8_codecvt_facet.cpp value_semantic.cpp variables_map.cpp winmain.cpp

OBJECTS=$(SOURCES:.cpp=.o)
OBJECTS := $(addprefix ./$(BIN_DIR)/,$(OBJECTS))

all: $(SOURCES) $(BIN_DIR)/$(EXECUTABLE)
	
$(BIN_DIR)/$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $@

$(BIN_DIR)/%.o : %.cpp
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $< -o $@
